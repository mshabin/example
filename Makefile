CFLAGS = -Wall

SOURCES = example.c
OBJECTS = $(SOURCES:.c=.o)
EXECUTABLE = example

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) ${LDFLAGS} -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)

